#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Just an example to draw a path line and flip it, with markers.
#  Requires Inkscape 1.2+ -->
# #############################################################################

import inkex
from inkex import PathElement, Circle, Marker


def make_path_line(self, points_list, parent, end_marker_color):

    marker_id = f'circle_marker{end_marker_color}'

    make_circle_marker(self, marker_id, end_marker_color)

    marker_url = f'url(#{marker_id})'

    my_path = PathElement()

    if self.options.end_markers_checkbox == 'false':
        marker_url = 'none'

    style = {'fill': 'none', 'stroke': 'red', 'stroke-width': '0.5', 'marker-end': marker_url}
    my_path_d = f'M {points_list[0][0]} {points_list[0][1]} L {points_list[-1][0]} {points_list[-1][1]}'

    my_path.set('d', my_path_d)
    my_path.style = style

    parent.append(my_path)


def make_circle_marker(self, marker_id, marker_fill_color):

    my_circle = Circle()
    my_circle.set('id', 'my_circle')
    my_circle.set('cx', 0)
    my_circle.set('cy', 0)
    my_circle.set('r', 2)
    my_circle.style['stroke'] = 'none'
    my_circle.style['fill'] = marker_fill_color
    my_circle.style['stroke-width'] = '0.1'

    circle_marker = Marker()
    circle_marker.set('id', marker_id)
    circle_marker.append(my_circle)

    self.svg.xpath('//svg:defs')[0].append(circle_marker)


class FlipLine(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--start_x_float", type=float, dest="start_x_float", default=10)
        pars.add_argument("--start_y_float", type=float, dest="start_y_float", default=10)
        pars.add_argument("--end_x_float", type=float, dest="end_x_float", default=10)
        pars.add_argument("--end_y_float", type=float, dest="end_y_float", default=10)

        pars.add_argument("--flip_horizontal_checkbox", type=str, dest="flip_horizontal_checkbox", default='false')
        pars.add_argument("--flip_vertical_checkbox", type=str, dest="flip_vertical_checkbox", default='false')

        pars.add_argument("--end_markers_checkbox", type=str, dest="end_markers_checkbox", default='true')

        pars.add_argument("--print_paths_checkbox", type=str, dest="print_paths_checkbox", default='true')


    def effect(self):

        start_x  = self.options.start_x_float
        start_y = self.options.start_y_float
        end_x = self.options.end_x_float
        end_y = self.options.end_y_float

        points_list1 = [[start_x, start_y], [end_x, end_y]]

        make_path_line(self, points_list1,  self.svg.get_current_layer(), 'green')

        # flip_horizontally
        if self.options.flip_horizontal_checkbox == 'true':
            points_list2 = [[end_x, start_y], [start_x, end_y]]
            make_path_line(self, points_list2,  self.svg.get_current_layer(), 'blue')
        else:
            points_list2 = 'None'

        # flip_vertically
        if self.options.flip_vertical_checkbox == 'true':
            points_list3 = [[start_x, end_y], [end_x, start_y]]
            make_path_line(self, points_list3,  self.svg.get_current_layer(), 'orange')
        else:
            points_list3 = 'None'

        # Print Paths to messagebox
        if self.options.print_paths_checkbox == 'true':
            inkex.errormsg(f'Path 1: {points_list1} Path 2: {points_list2} Path 3: {points_list3}')


if __name__ == '__main__':
    FlipLine().run()
